<?php
require 'app/config.php';
require 'app/test.php';

use App\Config;
use App\Test;

$mysql = new Config;
$all_tests = new Test;

?>

<?php session_start(); ?>
<?php include('includes/head.php') ?>



<body>
<!--<div class="bg-image"></div>-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
           <div class="card-wrapper">
               <div class="card">
                   <div class="card-body">
                       <form id="login-form" action="questions.php" method="post">
                           <div class="form-group text-center index-greetings">
                               <?php
                               if(empty($_SESSION['username'])){
                                   ?>
                                   <label>Enter your Name</label>
                                   <input id="#username" name="username" type="text" class="form-control" required>
                                   <?php
                               }else{
                                   ?>
                                   <div>
                                       <h3>Hello <?php echo $_SESSION['username']; ?> !</h3>
                                   </div>
                                   <?php
                               }
                               ?>
                           </div>
                           <div class="form-group text-center">
                               <label>Choose quiz</label>

                               <select id="#test" name="category_id" class="form-control" required>
                                   <option></option>
                                   <?php
                                   foreach ($all_tests->returnCategories() as $category){?>
                                       <option value="<?php echo $category['id']?>"><?php echo $category['title']; ?></option>
                                   <?php } ?>
                               </select>
                           </div>
                           <div class="form-group submit-button">
                               <input type="submit" value="Let`s go" class="form-control mt-3">
                           </div>
                       </form>
                   </div>
               </div>
           </div>
        </div>
    </div>
</div>
</body>
<?php include('includes/footer.php') ?>

<script>
    $(document).ready(function () {
        $('#login-form').validate();
    });
</script>
