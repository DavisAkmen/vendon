<?php

namespace App;
use mysqli;

class Config {
    /**
     * Config class
     * Class for mysql connection.
     * @author Davis Akmentins <d.akmentins@gmail.com>
     */

    /**
     * A public variables
     *
     * @var $connection object stores data for the class
     * @var $servername  string stores host name/IP
     * @var $dbusername  string stores username for database
     * @var $dbpassword  string stores password for database
     * @var $dbname string stores database name
     */
    public $connection;
    public $servername;
    public $dbusername;
    public $dbpassword;
    public $dbname;

    public function __construct()
    {
        /**
         * Enter your database connection info
         */

        $this->servername = "localhost";
        $this->dbusername = "database_username";
        $this->dbpassword = "database_password";
        $this->dbname = "database_name";
    }

    public function connect()
    {
        /**
         * Checks the connection to database
         * and if successful returns data
         *
         * @return object
         */

        $this->connection = new mysqli($this->servername, $this->dbusername, $this->dbpassword, $this->dbname);
        if ($this->connection->connect_error){
            die("Connection failed: " . $this->connection->connect_error);
        }
        return $this->connection;

    }
}