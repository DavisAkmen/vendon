<?php
namespace App;

class Question {

    /**
     * A public variable
     *
     * @var object for connection
     */
    public $connection;


    public function __construct()
    {
        /**
         * Includes Config class and
         * creates connection to database
         */
        $this->connection = new Config;
        $this->connection->connect();
    }


    /**
     * Returns all questions by given category
     * in ascending order
     * @var $question string
     * @return \mysqli_result
     */
    public function getQuestions($category_id)
    {
        $question = "SELECT * FROM questions WHERE category_id = '$category_id' ORDER BY questions.id ASC";
        return $this->connection->connect()->query($question);
    }


    /**
     * Returns all answers by given category
     * in ascending order
     * @var $answers string
     * @return object
     */
    public function getAnswers($question_id)
    {
        $answers = "SELECT * FROM answers WHERE question_id = '$question_id'";
        return $this->connection->connect()->query($answers);
    }

//    public function returnNextQuestion($category_id, $question_id)
//    {
//        $query_next_question = "SELECT * FROM questions WHERE category_id = '$category_id' AND id != '$question_id' ORDER BY questions.id ASC LIMIT 1";
//        $next_question = $this->connection->connect()->query($query_next_question);
//
//        $next_question_array = [];
//
//        foreach ($next_question as $value){
//            $next_question_array['id'] = $value['id'];
//            $next_question_array['name'] = $value['name'];
//            $next_question_array['category_id'] = $value['category_id'];
//        }
//        return $next_question_array;
//
//    }

    /**
     * Returns total amount of questions
     * @var $query_test string
     * @return array
     */
    public function returnAmountOfQuestions($category_id)
    {
        $query_tests = "SELECT * FROM questions WHERE category_id = '$category_id'";
        $tests = $this->connection->connect()->query($query_tests);

        $amount = [];
        foreach ($tests as $test){
            $num_of_rows = mysqli_num_rows($tests);
            $amount['amount'] = $num_of_rows;
        }

        return $amount;
    }

}
