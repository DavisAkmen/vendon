<?php
namespace App;

class Test {
    /**
     * A public variable
     *
     * @var object for connection
     */
    public $connection;

    public function __construct()
    {
        /**
         * Includes Config class and
         * creates connection to database
         */
        $this->connection = new Config;
        $this->connection->connect();
    }

    public function returnCategories()
    {
        /**
         * Queries the database
         *
         * @var $test_query string stores query to database
         * @return object
         */

        $test_query = "SELECT * FROM categories";
        return $this->connection->connect()->query($test_query);
    }

    /**
     * Creates test for each user
     * per category
     * @var $user_id int
     * @var $category_id int
     * @var $answer_id int
     * @var $question_id int
     * @var $is_correct int
     * @return object
     */
    public function createTest($user_id, $category_id, $answer_id, $question_id, $is_correct = null)
    {
        $create_test = "INSERT INTO tests (user_id,category_id,answer_id,question_id,is_correct) VALUES ('$user_id','$category_id','$answer_id','$question_id','$is_correct')";
        return $this->connection->connect()->query($create_test);
    }




















}