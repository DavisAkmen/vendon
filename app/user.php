<?php
namespace App;

class User {
    /**
     * A public variable
     *
     * @var object for connection
     */
    public $connection;


    public function __construct()
    {
        /**
         * Includes Config class and
         * creates connection to database
         */
        $this->connection = new Config;
        $this->connection->connect();


    }

    /**
     * Checks if user is already registered
     * and returns TRUE user exists
     * @var $username string
     * @return bool
     */
    public function isExistingUser($username)
    {
        $query_user = mysqli_query($this->connection->connect(), "select count(username) as count from users where username='$username'");
        $count = mysqli_fetch_array($query_user);
        if($count[0] != 0){
            return true;
        }else{
            return false;
        }

    }


    /**
     * creates user if it already doesnt exist
     * @var $username string
     * @return array
     */
    public function createUser($username)
    {

        $insert_user = "INSERT INTO users (username) VALUES ('$username')";
        return $this->connection->connect()->query($insert_user);

    }


//    public function updateUserTestName($username, $test_name)
//    {
//        $update_query = "UPDATE users SET test_name='$test_name' WHERE username='$username'";
//        return $this->connection->connect()->query($update_query);
//    }


    /**
     * Returns user when called
     * @var $username string
     * @var $category_id int that is an actual quiz id
     * @return array
     */
    public function getUser($username, $category_id)
    {
        /**
         * Queries database
         */
        $users = "SELECT * FROM users WHERE username='$username'";
        $user_from_database = $this->connection->connect()->query($users);

        /**
         * Stores data in an array
         */
        $logged_in_user_array = [];
        foreach ($user_from_database as $logged_in_user){
            $logged_in_user_array['user_id'] = $logged_in_user['id'];
            $logged_in_user_array['username'] = $logged_in_user['username'];
            $logged_in_user_array['category_id'] = $category_id;
        }
        return $logged_in_user_array;

    }


    /**
     * Checks if user is logged in and if not
     * by checking session.
     * returns to login message
     * If all data provided then created new user or logs
     * in an existing one
     */
    public function isLoggedIn()
    {
        if(!empty($_SESSION['username'])){
            $category_id = $_POST['category_id'];
            $logged_in_user = $this->getUser($_SESSION['username'], $category_id);
            return $logged_in_user;
        }else{
            if(!empty($_POST['username']) && $_POST['category_id']){
                $username = $_POST['username'];
                $category_id = $_POST['category_id'];
                $_SESSION['username'] = $username;
                $_SESSION['category_id'] = $category_id;

                if($this->isExistingUser($username)){
                    $existing_user = $this->getUser($username, $category_id);
                    return $existing_user;
                }else{
                    $this->createUser($username);
                    $created_user = $this->getUser($username, $category_id);
                    return $created_user;
                }
            }else{
                return false;
            }
        }
    }

    /**
     * Returns question answers by given category_id
     * @var $user_id int
     * @var $category_id int
     * @return array
     */
    public function getAllAnswers($user_id, $category_id)
    {
        $query = "SELECT * FROM tests WHERE user_id = '$user_id' AND category_id = '$category_id'";
        $summary =  $this->connection->connect()->query($query);

        $all_answers_array = [];
        foreach ($summary as $data){
            $num_of_questions = mysqli_num_rows($summary);
            $all_answers_array['all_answers'] = $num_of_questions;
        }

        return $all_answers_array;

    }

    /**
     * Collects all correct answers
     * @var $user_id int
     * @var $category_id int
     * @return array
     */
    public function getCorrectAnswers($user_id, $category_id)
    {
        $query = "SELECT * FROM tests WHERE user_id = '$user_id' AND category_id = '$category_id' AND is_correct = 1";
        $correct_answers = $this->connection->connect()->query($query);

        $correct_answers_array = [];
        foreach ($correct_answers as $data){
            $num_of_correct_answers = mysqli_num_rows($correct_answers);
            $correct_answers_array['correct_answers'] = $num_of_correct_answers;
        }

        return $correct_answers_array;
    }


    /**
     * Checks if test is already completed
     * @var $user_id int
     * @var $category_id int
     * @return array
     */
    public function isTestAlreadyCompleted($user_id, $category_id)
    {
        $query_tests = "SELECT * FROM tests WHERE user_id = '$user_id' AND category_id = '$category_id'";
        $tests = $this->connection->connect()->query($query_tests);

        $completed_tests = [];
        foreach ($tests as $test){
            $num_of_completed = mysqli_num_rows($tests);
            $completed_tests['completed'] = $num_of_completed;
        }

        return $completed_tests;
    }

    /**
     * Deletes existing quiz if already completed
     * by the same user
     * @var $user_id int
     * @var $category_id int
     * @return array
     */
    public function deleteIfExists($user_id, $category_id)
    {
        $delete_query = "DELETE FROM tests WHERE user_id = '$user_id' AND category_id = '$category_id'";
        return $this->connection->connect()->query($delete_query);
    }

    public function isNameOrQuizFieldsEmpty()
    {
        if($this->isLoggedIn()){
            if($_POST['category_id'] == ''){
                return false;
            }else{
                return true;
            }
        }else{
            if($_POST['username'] == '' || $_POST['category_id'] == ''){
                return false;
            }else{
                return true;
            }
        }
    }
































}