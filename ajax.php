<?php
session_start();

require 'app/test.php';
require 'app/config.php';
require 'app/question.php';
require 'app/user.php';

use App\User;
use App\Test;
use App\Question;

$tests = new Test;
$questions = new Question;
$user = new User;

/**
 * Collects data from the answered question
 * and stores each answer in database
 * by ajax request
 * @var $logged_in_user bool
 * @var $user_id int
 * @var $category_id int
 * @var $answer_id int
 * @var $question_id int
 * @var $is_correct int
 */


$logged_in_user = $user->isLoggedIn();
$user_id = $_POST['user_id'];
$category_id = $_POST['category_id'];

if($_POST['answer_id'] == ''){
    $answer_id = 0;
}else{
    $answer_id = $_POST['answer_id'];
}
$question_id = $_POST['question_id'];

if($_POST['is_correct'] == ''){
    $is_correct = 0;
}else{
    $is_correct = $_POST['is_correct'];
}

$tests->createTest($user_id, $category_id, $answer_id, $question_id, $is_correct);


