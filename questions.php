<?php
require 'app/config.php';
require 'app/user.php';
require 'app/question.php';
use App\User;
use App\Question;
session_start();
$user = new User;
$questions = new Question;

$empty_fields = $user->isNameOrQuizFieldsEmpty();
$logged_in_user = $user->isLoggedIn();
$category_id = $logged_in_user['category_id'];
$question_query =  $questions->getQuestions($logged_in_user['category_id']);
$completed = $user->isTestAlreadyCompleted($logged_in_user['user_id'], $category_id);

if($completed){
    $user->deleteIfExists($logged_in_user['user_id'], $category_id);
}


include 'includes/head.php';
?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <div class="quiz-card-wrapper">
                   <div class="quiz-card text-center">
                       <?php
                       if($logged_in_user && $empty_fields){
                           ?>
                           <form action="summary.php" method="post">
                           <?php
                           foreach ($question_query as $i => $question){
                               $num_of_rows = mysqli_num_rows($question_query);
                               $result_count = $i + 1;
                               ?>

                                   <div id="question<?php echo $i; ?>" class="question-class">
                                       <div class="row">
                                           <h1 class="question-title"><?php echo $question['name']; ?></h1>
                                           <input type="hidden" name="category_id" value="<?php echo $logged_in_user['category_id']; ?>">
                                           <?php
                                           foreach ($questions->getAnswers($question['id']) as $answer){
                                               ?>
                                               <div class="col-md-6">
                                                   <div class="answer-button-block">
                                                       <button type="button" class="answer-button btn btn-success" data-answer-id="<?php echo $answer['id'] ?>" data-is-correct="<?php echo $answer['is_correct'] ?>" >
                                                           <input type="hidden" id="is_correct" name="is_correct" value="<?php echo $answer['is_correct'] ?>">
                                                           <input type="hidden" id="answer_id" name="answer_id" value="<?php echo $answer['id'] ?>">
                                                           <?php echo $answer['name']; ?>
                                                       </button>
                                                   </div>
                                               </div>
                                               <?php
                                           }
                                           ?>
                                       </div>
                                       <?php if($result_count != $num_of_rows){
                                           ?>
                                           <div class="row">
                                               <div class="col-md-12">
                                                   <input type="hidden" name="question_id" value="<?php echo $question['id'] ?>">
                                                   <input type="hidden" name="user_id" value="<?php echo $logged_in_user['user_id']; ?>">
                                                   <input type="hidden" name="category_id" value="<?php echo $logged_in_user['category_id']; ?>">
                                                   <input type="hidden" name="step" value="<?php echo $i; ?>">
                                                   <button id="next<?php echo $i; ?>" class="next-button btn btn-success"
                                                           data-next="next<?php echo $i; ?>"
                                                           data-counter="<?php echo $i + 1; ?>"
                                                           value="0"
                                                           data-answer-id=""
                                                           data-id="<?php echo $question['id'] ?>"
                                                           data-user-id="<?php echo $logged_in_user['user_id']; ?>"
                                                           data-category-id="<?php echo $logged_in_user['category_id']; ?>"
                                                           data-step="<?php echo $i; ?>"
                                                           type="button">Next
                                                   </button>
                                               </div>
                                           </div>
                                       <?php
                                       }else{
                                           ?>
                                           <div class="row">
                                               <div class="col-md-12">
                                                   <input type="hidden" name="question_id" value="<?php echo $question['id'] ?>">
                                                   <input type="hidden" name="user_id" value="<?php echo $logged_in_user['user_id']; ?>">
                                                   <input type="hidden" name="category_id" value="<?php echo $logged_in_user['category_id']; ?>">
                                                   <input type="hidden" name="step" value="<?php echo $i; ?>">
                                                   <input type="hidden" id="finish-answer-id" name="answer_id" value="">
                                                   <input type="hidden" id="finish-is-correct" name="is_correct" value="">
                                                   <button id="Finish" class="finish-button btn btn-success"
                                                           value=""
                                                           data-answer-id=""
                                                           data-id="<?php echo $question['id'] ?>"
                                                           data-user-id="<?php echo $logged_in_user['user_id']; ?>"
                                                           data-category-id="<?php echo $logged_in_user['category_id']; ?>"
                                                           data-step="<?php echo $i; ?>"
                                                           type="submit">Finish
                                                   </button>
                                               </div>
                                           </div>
                                       <?php
                                       } ?>
                                       <?php
                                       $amount = $questions->returnAmountOfQuestions($category_id);


                                       $percent = 100 / $amount['amount'];
                                       echo number_format($percent * ($i + 1)).'%';
                                       ?>
                                       <input type="hidden" id="progress-bar-percent" value="<?php echo $percent; ?>">
                                       <input type="hidden" id="question-count" value="<?php echo $i; ?>">


                                       <div class="row">
                                           <div class="col-md-2"></div>
                                           <div class="col-md-8">
                                               <div class="progress">
                                                   <div id="vendon-progress-bar" class="progress-bar vendon-progress-bar vendon-progress-bar" role="progressbar" aria-valuenow="70"

                                                        aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent * ($i + 1) ; ?>%">
                                                       <span class="sr-only">70% Complete</span>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2"></div>
                                       </div>
                                   </div>

                               <?php
                           }
                           ?>
                           </form>
                               <?php
                           ?>
                           <?php
                       }else{
                           ?>
                           <div class="error-wrap">
                               <h3>Please enter your name AND choose quiz</h3>
                               <a href="/">Back</a>
                           </div>
                           <?php
                       }
                       ?>
                   </div>
               </div>
            </div>
        </div>
    </div>
</body>


<script>
    $('.question-class').addClass('hide');
    count=$('.questions').length;
    $('#question'+0).removeClass('hide');

    $('.next-button').on('click',function(){
        var element = $(this).attr('data-next');

        var last = parseInt(element.substr(element.length - 1));
        var next = last + 1;
        $('#question'+last).addClass('hide');

        $('#question'+next).removeClass('hide');

    });

    $(document).ready(function(){


        function fetch_question_data(question_id, is_correct, user_id, answer_id, category_id)
        {
            $.ajax({
                url:"ajax.php",
                method:"POST",
                data:{
                    question_id:question_id,
                    is_correct:is_correct,
                    user_id:user_id,
                    answer_id:answer_id,
                    category_id:category_id,

                },
                success:function(data)
                {
                    console.log(data)
                },
                error: function (error) {
                    console.log(error)
                }
                // dataType:"json"
            });
        }

        var next = $('.next-button');
        var finish = $('.finish-button');

        $('.answer-button').on('click', function () {
            var is_correct = $(this).data('is-correct');
            var answer_id = $(this).data('answer-id');
            next.attr('value', is_correct);
            next.attr('data-answer-id', answer_id);
            var is_correct_input = $('#finish-is-correct');
            var answer_id_input = $('#finish-answer-id');
            finish.attr('data-answer-id', answer_id);
            answer_id_input.attr('value', answer_id);
            is_correct_input.attr('value', is_correct);
        });

        var percent = $('#progress-bar-percent').val();
        var vendon_progress_bar = $('#vendon-progress-bar');
        vendon_progress_bar.css('width', percent+'%');

        next.on('click', function () {
            var question_id = $(this).data('id');
            var is_correct = $(this).val();
            var user_id = $(this).data('user-id');
            var answer_id = $(this).data('answer-id');
            var category_id = $(this).data('category-id');

            fetch_question_data(question_id, is_correct, user_id, answer_id, category_id);
        })
    });

</script>

