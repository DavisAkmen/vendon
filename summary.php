<?php
session_start();

require 'app/test.php';
require 'app/config.php';
require 'app/question.php';
require 'app/user.php';

use App\User;
use App\Test;
use App\Question;

$tests = new Test;
$questions = new Question;
$user = new User;

$logged_in_user = $user->isLoggedIn();
$question_id = $_POST['question_id'];
$user_id = $_POST['user_id'];
$category_id = $_POST['category_id'];

if($_POST['answer_id'] == ''){
    $answer_id = 0;
}else{
    $answer_id = $_POST['answer_id'];
}

if($_POST['is_correct'] == ''){
    $is_correct = 0;
}else{
    $is_correct = $_POST['is_correct'];
}

$tests->createTest($user_id, $category_id, $answer_id, $question_id, $is_correct);
$summary = $user->getAllAnswers($logged_in_user['user_id'], $logged_in_user['category_id']);
$correct_answers = $user->getCorrectAnswers($logged_in_user['user_id'], $logged_in_user['category_id']);

include 'includes/head.php';
?>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="quiz-card-wrapper">
                <div class="quiz-card text-center">
                   <div class="row">
                       <div class="col-md-12">
                           <?php
                           if($logged_in_user){
                               ?>
                               <div class="summary">
                                   <h1 class="question-title">Thanks <?php echo $logged_in_user['username']; ?> !</h1>
                                   <p>You answered correctly to <?php if($correct_answers){echo $correct_answers['correct_answers'];}else{echo '0';} ?> out of <?php echo $summary['all_answers'] ?> questions</p>
                                   <a href="/">Back</a>
                               </div>
                               <?php
                               ?>
                               <?php
                           }else{
                               ?>
                               <h1>Please log in</h1>
                               <?php
                           }
                           ?>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
