<!DOCTYPE html>
<html lang="en">
<head>
    <title>Vendon test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="includes/libraries/bootstrap/css/bootstrap-3.3.7.min.css">
    <script src="includes/libraries/jquery/jquery-3.3.1.min.js"></script>
    <script src="includes/libraries/bootstrap/js/bootstrap-3.3.7.min.js"></script>
    <link rel="stylesheet" type="text/css" href="includes/css/styles.css">
</head>